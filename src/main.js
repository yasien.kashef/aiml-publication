/**
 * The main.js is the starting point of the Web-Application.
 * It imports the needed Framework Vue.js, the main Vue-Application App, router.js & store.js.
 * Finally it creates the Vue Application, renders and mounts the whole application to the index.
 * @author Yasien Kashef
 * @version 1.0.0
 * @requires Vue : to use the Vue.js-Framework.
 * @requires App.vue : to access the App.vue, the main module of the ControlCenter.
 * @requires router.js : to use Router of Vue.js
 * @requires store.js : to use Store of Vue.js
 */
import Vue from "vue";
import App from "./App.vue";
import router from "./router/router";
import store from "./store/store";

Vue.config.productionTip = false; // Prevents the production tip on Vue startup

/**
 * The main Vue Application. Gets mounted in the <div id="app"> in index.html
 * @requires store.js
 * @requires router.js
 * @requires App.vue
 */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
