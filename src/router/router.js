/**
 * The router.js is responsible for the Routing Management of the Web-Application.
 * It uses Vue Router the official router for Vue.js.
 * It imports the needed Framework Vue.js, Vue Router and the RationalsView.
 * It creates all routes to all views and does the loading and import of the respective views.
 * @author Yasien Kashef
 * @version 1.0.0
 * @requires Vue : to use the Vue.js-Framework.
 * @requires VueRouter : to use Vue Router.
 * @requires RationalsView : to use the Rationals.vue view
 */
import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";

Vue.use(VueRouter); // to use the plugin vue-router globally

/**
 * The Router of the Web-Application, which gets included in the main vue-application.
 * The paths and routings are virtual, they dont correspond exactly to the views and files.
 * Just the component imports needs to point to the correct path of the view.
 * @since 1.0.0
 * @requires RationalsView.vue : to use the RationalsView.vue view, as the default view.
 */

export default new VueRouter({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/Topic",
      name: "Topic",
      component: () =>
        import(/* webpackChunkName: "Topic" */ "@/views/Topic.vue")
    },
    {
      path: "/Subtopic",
      name: "Subtopic",
      component: () =>
        import(/* webpackChunkName: "Subtopic" */ "@/views/Subtopic.vue")
    }
  ]
});
